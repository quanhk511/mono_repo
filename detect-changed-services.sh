#!/bin/bash -e
ARRAY=( "lhiot-admin:$AD_BUCKET"
        "lhiot-register:$REGISTER_BUCKET"
        "lhiot-mobile:$MOBILE_BUCKET"
      )

SH=$(cd `dirname $BASH_SOURCE` && pwd)
COMMIT_RANGE="HEAD HEAD~1"
changed_folders=`git diff --name-only ${COMMIT_RANGE} | grep / | awk 'BEGIN {FS="/"} {print $1}' | uniq`

for folder in $changed_folders
do

  for s3_url in "${ARRAY[@]}" ; do

      KEY="${s3_url%%:*}"
      VALUE="${s3_url##*:}"

      if [ "$folder" == "$KEY" ]; then

        eho PRE-INSTALL;
        cd $SH/$folder && npm i;
        eho BUILD;
        npm run build;
        aws s3 sync ./build $VALUE

  done

done